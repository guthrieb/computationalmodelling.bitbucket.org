import cv2
import numpy as nump

vid = cv2.VideoCapture('video.avi')
fourcc = cv2.VideoWriter_fourcc(*'XVID')
vwo = cv2.VideoWriter('gray_video.avi',fourcc, 25.0,(int(vid.get(3)),int(vid.get(4))),False)

while(vid.isOpened()):

	ret, frame = vid.read()
	if(ret):

		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		vwo.write(gray)
		cv2.imshow('video',frame)
		cv2.waitKey(25)
	else:
		break

vwo.release()
vid.release()
cv2.destroyAllWindows()