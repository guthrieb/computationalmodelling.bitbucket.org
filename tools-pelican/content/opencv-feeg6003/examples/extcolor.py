import cv2 as cv
import numpy as np
image = cv.imread('logo.png')
image_hsv = cv.cvtColor(image,cv.COLOR_BGR2HSV)
blue = np.uint8([[[255,0,0]]])
hsv_blue = cv.cvtColor(blue,cv.COLOR_BGR2HSV)
lower_blue = np.array(hsv_blue[0,0,:])
lower_blue[0] = lower_blue[0] - 10
upper_blue = np.array(hsv_blue[0,0,:])
upper_blue[0] = upper_blue[0] + 10
mask = cv.inRange(image_hsv,lower_blue,upper_blue)
res = cv.bitwise_and(image,image,mask = mask)
cv.imshow('image',image)
cv.imshow('mask',mask)
cv.imshow('result',res)
cv.waitKey(0)
cv.destroyAllWindows()

