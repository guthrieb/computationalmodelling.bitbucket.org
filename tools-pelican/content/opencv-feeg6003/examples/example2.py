import cv2
import numpy as nump

vid = cv2.VideoCapture('video.avi')

#Define the fourcc code. For ubuntu it is recommended to use XVID.
fourcc = cv2.VideoWriter_fourcc(*'XVID')
# Create the VideoWriter object with color flag as False because
# the video is going to be converted to grayscale at the output
vwo = cv2.VideoWriter('output.avi',fourcc, 25.0,(int(vid.get(3)),int(vid.get(4))),False)

# Check if the video has been opened correctly
while(vid.isOpened()):
    ret, frame = vid.read()

    # If the frame is ok, the video is modifiec and the code shows
    # the original video
    if ret:     
     # Do some modifications to the video, in this case flip it and
     # change it to grayscale
     gray = cv2.flip(frame, 0)
     gra = cv2.cvtColor(gray, cv2.COLOR_BGR2GRAY)

     # Write the video out to a new file
     vwo.write(gra)

     # Show the original video
     cv2.imshow('GenuineVideo',frame)

     # Do all the stuff every 25 ms
     cv2.waitKey(25)
    else:
    	break

vid.release()
vwo.release()
cv2.destroyWindow('GenuineVideo')

#Display the new video with its modifications
show = cv2.VideoCapture('output.avi')

while(show.isOpened()):
	ret1, frame1 = show.read()
	if(ret1):
		cv2.imshow('ModVideo',frame1)
		cv2.waitKey(25)
	else:
		break

show.release()
cv2.destroyAllWindows()